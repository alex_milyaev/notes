import Vue from "vue";
import Vuex from "vuex";

import Notes from "./notes.js";

Vue.use(Vuex);

export default new Vuex.Store({
  modules: {
    Notes
  }
});
