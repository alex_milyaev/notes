import { db } from "../main.js";

export default {
  state: {
    status: ""
  },
  mutations: {
    SET_STATUS(state, payload) {
      state.status = payload;
    }
  },
  actions: {
    CREATE_NOTE({ commit }, payload) {
      let note = payload;
      return new Promise((resolve, reject) => {
        db.collection("notes")
          .add(note)
          .then(response => {
            let id = response.id;
            note.id = id;
            resolve(note);
            commit("SET_STATUS", "Add Notes");
          })
          .catch(error => {
            reject(error);
            console.log("Error", error);
          });
      });
    },
    LOAD_NOTE({ commit }, payload) {
      return new Promise((resolve, reject) => {
        var docRef = db.collection("notes").doc(payload);
        docRef
          .get()
          .then(function(doc) {
            if (doc.exists) {
              let note = doc.data();
              commit("SET_STATUS", "Add Notes");
              resolve(note);
            } else {
              // doc.data() will be undefined in this case
              console.log("No such document!");
            }
          })
          .catch(function(error) {
            console.log("Error getting document:", error);
            reject(error);
          });
      });
    },
    DELETE_NOTE({ commit }, payload) {
      db.collection("notes")
        .doc(payload)
        .delete()
        .then(function() {
          console.log("Document successfully deleted!");
          commit("SET_STATUS", "Delete Notes");
        })
        .catch(function(error) {
          console.error("Error removing document: ", error);
        });
    },
    UPDATE_NOTE({ commit }, payload) {
      return new Promise((resolve, reject) => {
        db.collection("notes")
          .doc(payload.id)
          .update(payload)
          .then(function() {
            console.log("Document successfully updated!", payload);
            commit("SET_STATUS", "Document successfully updated!");
            resolve(payload);
          })
          .catch(function(error) {
            console.error("Error removing document: ", error);
            reject(error);
          });
      });
    }
  },
  getters: {}
};
