import Vue from "vue";
import VueRouter from "vue-router";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "home",
    meta: { layout: "default" },
    component: () => import("../views/Home.vue")
  },
  {
    path: "/note/:id",
    name: "note",
    meta: { layout: "default" },
    component: () => import("../views/Note.vue")
  }
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes
});

export default router;
