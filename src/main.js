import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import "./registerServiceWorker";

/* firebase */
import firebase from "firebase";
import VueFirestore from "vue-firestore";

Vue.use(VueFirestore);

const firebaseApp = firebase.initializeApp({
  apiKey: "AIzaSyDG3_lvyIJbbNWJ_7gOevfcsIRBnvDQu7I",
  authDomain: "notes-60434.firebaseapp.com",
  databaseURL: "https://notes-60434.firebaseio.com",
  projectId: "notes-60434",
  storageBucket: "notes-60434.appspot.com",
  messagingSenderId: "517525430836",
  appId: "1:517525430836:web:1f409ce32642f1bfb6a061"
});

export const db = firebaseApp.firestore();

//Layout & Styles
import Default from "./layouts/Default.vue";
Vue.component("default-layout", Default);
import "./styles/index.scss";

Vue.config.productionTip = false;

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount("#app");
